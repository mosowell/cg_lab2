﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Graphics_lab_2
{
    public partial class Form1 : Form
    {
        Bitmap bitmap;
        Point point1; Point point2;
        bool flag;
        Graphics g;
        Pen blackBrush = new Pen(new SolidBrush(Color.Black), 2);

        public Form1()
        {
            InitializeComponent();
            bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            flag = false;

        }

        public void drowLine(int x1, int y1, int x2, int y2)
        {
            Pen redBrush = new Pen(new SolidBrush(Color.Black), 2);
            // Длина отрезка по X
            int deltaX = Math.Abs(x2 - x1);
            // Длина отрезка по Y
            int deltaY = Math.Abs(y2 - y1);
            int signX = x1 < x2 ? 1 : -1;
            int signY = y1 < y2 ? 1 : -1;
            int error = deltaX - deltaY;

            g.DrawRectangle(redBrush, x2, y2, 1, 1);

            while (x1 != x2 || y1 != y2)
            {
                g.DrawRectangle(redBrush, x1, y1, 1, 1);
                int error2 = error * 2;

                if (error2 > -deltaY)
                {
                    error -= deltaY;
                    x1 += signX;
                }
                if (error2 < deltaX)
                {
                    error += deltaX;
                    y1 += signY;
                }
            }
        }
        // Рисовать прямоугольник
        private void DrawRectangle(Point point1, Point point2) 
        {
            // Верхняя линия
            drowLine(point1.X, point1.Y, point2.X, point1.Y);
            // Нижняя линия
            drowLine(point1.X, point2.Y, point2.X, point2.Y);
            // Левая линия
            drowLine(point1.X, point1.Y, point1.X, point2.Y);
            // Правая линия
            drowLine(point2.X, point1.Y, point2.X, point2.Y);
        }

        // Рисование пикселя одновременно с 4 сторон элипса
        void Pixel4(int x, int y, int _x, int _y) 
                                                               
        {
            // (свер bnху, снизу, слева, справа)
            g.DrawRectangle(blackBrush, x + _x, y + _y, 1, 1);
            g.DrawRectangle(blackBrush, x + _x, y - _y, 1, 1);
            g.DrawRectangle(blackBrush, x - _x, y - _y, 1, 1);
            g.DrawRectangle(blackBrush, x - _x, y + _y, 1, 1);
        }

        // Функция рисования элипса: x,y - центр элипса, а,b - длины осей x y центр
        void DrawElipse(int x, int y, int a, int b) 
        {
            // Компонента x откуда начинаем рисовать 
            int _x = 0;
            // Компонента y
            int _y = b;
            // a ^ 2, a - большая полуось
            int a_sqr = a * a;
            // b^2, b - малая полуось
            int b_sqr = b * b;
            // Функция координат точки (x+1, y-1/2)
            int delta = 4 * b_sqr * ((_x + 1) * (_x + 1)) + a_sqr * ((2 * _y - 1) * (2 * _y - 1)) - 4 * a_sqr * b_sqr;
            // Первая часть дуги (дуга это четверть элипса), где часть - половинка дуги (в данном случае двигаемся по горизонтали)
            while (a_sqr * (2 * _y - 1) > 2 * b_sqr * (_x + 1)) 
            {
                Pixel4(x, y, _x, _y);
                // Изменяем х на 1
                _x++;
                // Если точка между пикселями ближе к текущему по вертикали, оставляем Y таким же
                if (delta < 0)
                {
                    delta += 4 * b_sqr * (2 * _x + 3);
                }
                // Если точка между пикселями ближе к следующему по вертикали, чем к текущему, изменяем точку на 1 по y
                else
                {
                    delta = delta - 8 * a_sqr * (_y - 1) + 4 * b_sqr * (2 * _x + 3);
                    _y--;
                }
            }
            // Функция координат точки (x+1/2, y-1)
            delta = b_sqr * ((2 * _x + 1) * (2 * _x + 1)) + 4 * a_sqr * ((_y + 1) * (_y + 1)) - 4 * a_sqr * b_sqr;
            // Вторая часть дуги, если не выполняется условие первого цикла, значит выполняется a^2(2y - 1) <= 2b^2(x + 1), в данном случае двигаемся по вертикали
            while (_y + 1 != 0) 
            {
                Pixel4(x, y, _x, _y);
                _y--;
                // Если точка между пикселями ближе к текущему по горизонтали, оставляем x таким же
                if (delta < 0) 
                {
                    delta += 4 * a_sqr * (2 * _y + 3);
                }
                // Если точка между пикселями ближе к следующему по горизонтали, чем к текущему, изменяем точку на 1 по х
                else
                {
                    delta = delta - 8 * b_sqr * (_x + 1) + 4 * a_sqr * (2 * _y + 3);
                    _x++;
                }
            }
        }
        private void pictureBox1_DownClick(object sender, MouseEventArgs e) 
        {
            // Усли нет рисунка
            if (!flag)
            {
                // Сохраняем точку
                point1 = new Point(e.X, e.Y);

            }


        }

        private void pictureBox1_UpClick(object sender, MouseEventArgs e)
        {
            // Если нет рисунка
            if (!flag)
            {
                g = Graphics.FromImage(bitmap);
                // Рисунок есть
                flag = true;
                // Сохраняем вторую точку
                point2 = new Point(e.X, e.Y);
                // Рисуем прямоугольник
                DrawRectangle(point1, point2);
                // Абсцисса центра 
                var cx = Convert.ToInt32(Math.Abs((point2.X + point1.X) / 2));
                // Ордината центра
                var cy = Convert.ToInt32(Math.Abs((point2.Y + point1.Y) / 2));
                // Сохраняем центр
                var circleCenter = new Point(cx, cy);
                // Длина элипса по оси ординат
                var circleLenghtY = Convert.ToInt32(Math.Abs((point2.Y - point1.Y) / 2));
                // Длина элипса по оси абсцисс
                var circleLenghtX = Convert.ToInt32(Math.Abs((point2.X - point1.X) / 2));
                // Рисуем элипс
                DrawElipse(circleCenter.X, circleCenter.Y, circleLenghtX, circleLenghtY);
            }
            // Если рисунок уже есть
            else
            {
                // Очищаем область
                g.Clear(Color.White);
                // Рисунка нет
                flag = false;
            }
            pictureBox1.Image = bitmap;
        }

    }
}
